import requests
import time
import json
import logging

def alert(event,config,thisHost,timestamp):
  logger = logging.getLogger('main') 
  
  ## Define payload
  payload = {
    "username" : "Docker Event Monitor",
    "attachments" : [
      {
        "fallback": "Details",
        "pretext": "Details",
        "fields": [
          {
            "title": "Host",
            "value": thisHost,
            "short": True
          },
          {
            "title": "Type",
            "value": event['Type'],
            "short": True
          },
          {
            "title": "Time",
            "value": timestamp,
            "short": True
          },
          {
            "title": "Action",
            "value": event['Action'],
            "short": True
          },
          {
            "title": "ID",
            "value": event['Actor']['ID'],
            "short": False
          }
        ]
      }
    ]
  }

  ## Append name to payload if exists
  if 'name' in event['Actor']['Attributes']:
    nameField = {
      "title": "Name",
      "value": event['Actor']['Attributes']['name'],
      "short": False
    }
    payload['attachments'][0]['fields'].append(nameField)

  ## Append tags to payload if exists
  if 'tags' in config['settings']:
    tags = ", ".join([str(x) for x in config['settings']['tags']])
    tagsField = {
      "title": "Tags",
      "value": tags,
      "short": False
    }
    payload['attachments'][0]['fields'].append(tagsField)

  ## Perform request
  try:
    requests.post(
      config['integrations']['slack']['url'], 
      data = json.dumps(payload),
      headers = {'Content-Type': 'application/json'}
    )

  except requests.exceptions.RequestException as e:
    logger.error('{}: {}'.format(__name__,e))