import sys
import yaml
import logging

def load():
  ## Load config from file
  try:
    logger = logging.getLogger('main')
    config = yaml.load(open('conf.yml', 'r'))
  except:
    logger.error('Unable to load configuration - shutting down')
    sys.exit(1)

  ## Validate config
  if config:
    ## Settings
    if 'settings' in config:
      if 'logging' not in config['settings']:
        config['settings'] = {'logging':'info'}
      if 'exclusions' in config['settings'] and 'inclusions' in config['settings'] :
        logger.warn('Both exclusions and inclusions specified in config. This is most likley an error.')
    else:
      logger.warn('Settings not defined - loading defaults')
      config['settings'] = {'logging':'info'}

    ## Events
    if 'events' in config:
      pass
    else:
      logger.warn('No events have been specified so no alerts will be triggered')
      config['events'] = {}
    ## Integrations
    if 'integrations' in config:
      pass
    else:
      logger.warn('No integrations have been specified so no alerts will be triggered')
      config['integrations'] = {}
    ## Return
    return config
  else:
    logger.error('Unable to load configuration - shutting down')
    sys.exit(1)


  